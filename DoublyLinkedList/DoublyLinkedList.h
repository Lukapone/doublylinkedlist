#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H


#include<iostream>
#include"DoubleListNode.h"
#include"DListIterator.h"

//DoublyLinkedList to model a list class //http://www.cplusplus.com/articles/Lw6AC542/
template <class Datatype>
class DoublyLinkedList
{
private:
	//member variables
	DoubleListNode<Datatype>* m_startPtr;
	DoubleListNode<Datatype>* m_endPtr;
	int m_count;

public:
	typedef DListIterator<Datatype> iterator;

	DoublyLinkedList();
	~DoublyLinkedList();

	bool isEmpty() const;
	//inserts and deletes at begin and end
	void insertBegin(Datatype&& p_data);
	void insertBegin(Datatype& p_data);
	void insertEnd(Datatype&& p_data);
	void insertEnd(Datatype& p_data);

	void deleteBegin();
	void deleteEnd();

	//geters
	DoubleListNode<Datatype>* getStartPtr() const;
	DoubleListNode<Datatype>* getEndPtr() const;
	int getCount() const;

	//task functions
	void insertAtPosition(int position,Datatype&& p_data);
	void insertAtPosition(int position, Datatype& p_data);
	void deleteAtPosition(int p_position);
	void delIter(iterator toDel);

	iterator begin() const;
	iterator reverseBegin() const;
	iterator end() const;
	
};

template < class Datatype>
DoublyLinkedList<Datatype>::DoublyLinkedList() : m_startPtr(nullptr), m_endPtr(nullptr), m_count(0)
{
}
//destructor is on complexity order O(n) because we have to delete each node separately
template < class Datatype>
DoublyLinkedList<Datatype>::~DoublyLinkedList()
{
	if (!isEmpty()) // List is not empty
	{
		DoubleListNode<Datatype> *currentPtr = m_startPtr;
		DoubleListNode<Datatype> *tempPtr{ nullptr }; //initialized

		while (currentPtr != nullptr) // delete remaining nodes
		{
			tempPtr = currentPtr; //change the one you want to delete
			currentPtr = currentPtr->getNextNode(); //change the current to the next
			delete tempPtr; 
		}
	}
}

//functions that returns iterator for moving between nodes
//typename to reasure compiler that we are using type
template < class Datatype>
typename DoublyLinkedList<Datatype>::iterator DoublyLinkedList<Datatype>::begin() const
{
	return iterator(m_startPtr);
}

//typename to reasure compiler that we are using type
template < class Datatype>
typename DoublyLinkedList<Datatype>::iterator DoublyLinkedList<Datatype>::reverseBegin() const
{
	return iterator(m_endPtr);
}

template < class Datatype>
typename DoublyLinkedList<Datatype>::iterator DoublyLinkedList<Datatype>::end() const
{
	return iterator(nullptr);//be carefull what is end
}

//check if the list is empty
template <class Datatype>
bool DoublyLinkedList<Datatype>::isEmpty() const
{
	if (m_startPtr == nullptr && m_endPtr == nullptr) //if the start pointer and end pointer are NULL then the list is empty
		return true;
	else
		return false;
}

//function which will insert node at the beginning of the list
//the complexity of this function is O(c) because it does not matter how big is the list
template <class Datatype>
void DoublyLinkedList<Datatype>::insertBegin(Datatype&& p_data)
{
	if (isEmpty()) //if the list is empty create first element of the list
	{
		//create new node
		m_startPtr = m_endPtr = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data));
	}
	else //if node(s) exist in list insert additional object before the first
	{
		DoubleListNode<Datatype> * newNode = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data));
		m_startPtr->setPrevNode(newNode);	 //set the previous node of the start to be the new one
		newNode->setNextNode(m_startPtr);	 //the next pointer of the new node points to the node that was previously first
		m_startPtr = newNode;					 //the pointer for the new node is now the starting node
	}
	m_count++;
}

//function which will insert node at the beginning of the list
//the complexity of this function is O(c) because it does not matter how big is the list
template <class Datatype>
void DoublyLinkedList<Datatype>::insertBegin(Datatype& p_data)
{
	if (isEmpty()) //if the list is empty create first element of the list
	{
		//create new node
		m_startPtr = m_endPtr = new DoubleListNode<Datatype>(p_data);
	}
	else //if node(s) exist in list insert additional object before the first
	{
		DoubleListNode<Datatype> * newNode = new DoubleListNode<Datatype>(p_data);
		m_startPtr->setPrevNode(newNode);	 //set the previous node of the start to be the new one
		newNode->setNextNode(m_startPtr);	 //the next pointer of the new node points to the node that was previously first
		m_startPtr = newNode;					 //the pointer for the new node is now the starting node
	}
	m_count++;
}

//delete the element at the beginning of the linked list
//the complexity of this function is O(c) because it does not matter how big is the list
template <class Datatype>
void DoublyLinkedList<Datatype>::deleteBegin()
{
	if (isEmpty())
	{
		std::cout << "The list is empty" << std::endl;
	}
	else
	{
		if (m_count == 1)//if we have last element set the beginning and end to be null pointers
		{
			delete m_startPtr;
			m_startPtr = m_endPtr = nullptr;
		}
		else
		{
			DoubleListNode<Datatype> *tempPtr{ nullptr };
			tempPtr = m_startPtr;
			m_startPtr = m_startPtr->getNextNode(); //set the next node to be the start
			m_startPtr->setPrevNode(nullptr);  //set the beginning to 0
			delete tempPtr;
			std::cout << "delete called" << std::endl;	
		}
		m_count--;
	}
}


//function which will insert node at the beginning of the list
//the complexity of this function is O(c) because it does not matter how big is the list
template <class Datatype>
void DoublyLinkedList<Datatype>::insertEnd(Datatype&& p_data)
{
	if (isEmpty()) //if the list is empty create first element of the list
	{
		//create new node
		m_startPtr = m_endPtr = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data));
	}
	else //if node(s) exist in list insert additional after the last
	{
		DoubleListNode<Datatype> * newNode = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data));
		m_endPtr->setNextNode(newNode); //the current last node's next pointer points to the new node
		newNode->setPrevNode(m_endPtr);	//node we appendet have pointer to the previous
		m_endPtr = newNode;				//the new node is now the last node in the list		
	}
	m_count++;
}

//function which will insert node at the beginning of the list
//the complexity of this function is O(c) because it does not matter how big is the list
template <class Datatype>
void DoublyLinkedList<Datatype>::insertEnd(Datatype& p_data)
{
	if (isEmpty()) //if the list is empty create first element of the list
	{
		//create new node
		m_startPtr = m_endPtr = new DoubleListNode<Datatype>(p_data);
	}
	else //if node(s) exist in list insert additional after the last
	{
		DoubleListNode<Datatype> * newNode = new DoubleListNode<Datatype>(p_data);
		m_endPtr->setNextNode(newNode); //the current last node's next pointer points to the new node
		newNode->setPrevNode(m_endPtr);	//node we appendet have pointer to the previous
		m_endPtr = newNode;				//the new node is now the last node in the list		
	}
	m_count++;
}

//delete the last element
//the complexity of this function is O(c) because it does not matter how big is the list
template <class Datatype>
void DoublyLinkedList<Datatype>::deleteEnd()
{
	if (isEmpty())
	{
		std::cout << "The list is empty" << std::endl;
	}
	else
	{
		if (m_count == 1)//if we have last element set the beginning and end to be null pointers
		{
			delete m_startPtr;
			m_startPtr = m_endPtr = nullptr;
		}
		else
		{
			DoubleListNode<Datatype> *tempPtr{ nullptr };
			tempPtr = m_endPtr;
			m_endPtr = m_endPtr->getPrevNode(); //set the prev node to be the end
			m_endPtr->setNextNode(nullptr);  //set the beginning to 0
			delete tempPtr;
			//std::cout << "delete called" << std::endl;
		}
		m_count--;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//task functions
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//function that inserts data between two nodes at the position we wanted and reconect the nodes
//to find the position which we want is an O(n) operation
//once we fount the position the insert is O(c) operation
//overall the insert at position is an O(c) complexity operation because we wont move any elements of the list
template <class Datatype>
void DoublyLinkedList<Datatype>::insertAtPosition(int position, Datatype&& p_data)
{
	if (position < 0)
	{
		std::cout << "Invalid negative position" << std::endl;
	}
	else
	{
		if (isEmpty()) //if the list is empty create first element of the list
		{
			//create new node
			m_startPtr = m_endPtr = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data));
			m_count++;
		}
		else
		{
			if (position == 0)
			{
				this->insertBegin(std::forward<Datatype>(p_data));
			}
			else if (position >= m_count)
			{
				this->insertEnd(std::forward<Datatype>(p_data));
			}
			else
			{
				iterator iter = this->begin();
				iter = iter + position; //move the iterator to the position you want
				
				//structure of constructor  p_nextNode  p_prevNode
				//create node full constructor next node is the current iter and previous is iter--
				DoubleListNode<Datatype> * newNode = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data), iter.getNodePtr(), iter.getPrevPtr());
				//link the nodes at the position				
				iter.setPrevPtr(newNode);//set the link at the position of iter		
				iter--;
				iter--;//we have to move twice because of the node we just appendet before
				iter.setNextPtr(newNode);//set the link before the position
				m_count++;
			}	
		}
	}
	
}

//function that inserts data between two nodes at the position we wanted and reconect the nodes
//to find the position which we want is an O(n) operation
//once we fount the position the insert is O(c) operation
//overall the insert at position is an O(c) complexity operation because we wont move any elements of the list
template <class Datatype>
void DoublyLinkedList<Datatype>::insertAtPosition(int position, Datatype& p_data)
{
	if (position < 0)
	{
		std::cout << "Invalid negative position" << std::endl;
	}
	else
	{
		if (isEmpty()) //if the list is empty create first element of the list
		{
			//create new node
			m_startPtr = m_endPtr = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data));
			m_count++;
		}
		else
		{
			if (position == 0)
			{
				this->insertBegin(std::forward<Datatype>(p_data));
			}
			else if (position >= m_count)
			{
				this->insertEnd(std::forward<Datatype>(p_data));
			}
			else
			{
				iterator iter = this->begin();
				iter = iter + position; //move the iterator to the position you want

				//structure of constructor  p_nextNode  p_prevNode
				//create node full constructor next node is the current iter and previous is iter--
				DoubleListNode<Datatype> * newNode = new DoubleListNode<Datatype>(std::forward<Datatype>(p_data), iter.getNodePtr(), iter.getPrevPtr());
				//link the nodes at the position				
				iter.setPrevPtr(newNode);//set the link at the position of iter		
				iter--;
				iter--;//we have to move twice because of the node we just appendet before
				iter.setNextPtr(newNode);//set the link before the position
				m_count++;
			}
		}
	}

}


//function which delete the node at position we want
//to find the position which we want is an O(n) operation
//once we fount the position the delete is O(c) operation
//overall the delete at position is an O(c) complexity operation
template <class Datatype>
void DoublyLinkedList<Datatype>::deleteAtPosition(int p_position)
{
	if (p_position < 0)
		{
			std::cout << "Invalid negative position" << std::endl;
		}
		else
		{
			if (isEmpty())
			{
				std::cout << "The list is empty" << std::endl;
			}
			else
			{
				if (p_position == 0)
				{
					this->deleteBegin();
				}
				else if (p_position >= m_count-1)//be carefull with count if we want to delete last is one less than count
				{
					this->deleteEnd();
				}
				else
				{

					if (m_count == 1)//if we have last element set the beginning and end to be null pointers
					{
						delete m_startPtr;
						m_startPtr = m_endPtr = nullptr;
					}
					else
					{
						DoubleListNode<Datatype> *tempPtr{ nullptr };
						iterator iter = this->begin();
						iter = iter + p_position; //move the iterator to the position you want
						tempPtr = iter.getNodePtr();
						iter++;
						iter.setPrevPtr(tempPtr->getPrevNode()); //move forward set the pointer to 2 nodes back
						iter--;									//move to the one we want to set
						iter.setNextPtr(tempPtr->getNextNode()); //set the next of the node to be 2 nodes forward
						delete tempPtr;
						//std::cout << "delete at position called" << std::endl;
					}
					m_count--;
				}
				
			}
		}
}

//delete the element which we pass as iterator
//the complexity of the operation is O(c)
template <class Datatype>
void DoublyLinkedList<Datatype>::delIter(typename DoublyLinkedList<Datatype>::iterator iter)
{
	if (iter.getNodePtr() == m_startPtr)
	{
		this->deleteBegin();
	}
	else if (iter.getNodePtr() == m_endPtr)
	{
		this->deleteEnd();
	}
	else
	{
		if (m_count == 1) //if we have last element set the beginning and end to be null pointers
		{
			delete m_startPtr;
			m_startPtr = m_endPtr = nullptr;
		}
		else
		{
			DoubleListNode<Datatype> *tempPtr{ nullptr };
			tempPtr = iter.getNodePtr();			
			iter++;			
			iter.setPrevPtr(tempPtr->getPrevNode()); //move forward set the pointer to 2 nodes back			
			iter--;									//move to the one we want to set
			iter.setNextPtr(tempPtr->getNextNode()); //set the next of the node to be 2 nodes forward
			delete tempPtr;
			
		}
		m_count--;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//GETTERS AND SETTERS all are O(c) complexity
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class Datatype>
DoubleListNode<Datatype>* DoublyLinkedList<Datatype>::getStartPtr() const
{
	return m_startPtr;
}


template <class Datatype>
DoubleListNode<Datatype>* DoublyLinkedList<Datatype>::getEndPtr() const
{
	return m_endPtr;
}

template <class Datatype>
int DoublyLinkedList<Datatype>::getCount() const
{
	return m_count;
}









#endif

