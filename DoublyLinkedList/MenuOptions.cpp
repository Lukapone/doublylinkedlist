//this file contains functions that prints options for user and take his input

#include"MenuOptions.h"
#include"UserInput.h"


using std::cin;		using std::cout;
using std::endl;	using std::string;
using std::vector;
//enum with options bindet to sensible names
enum MenuOptions
{
	INSERT_NEW_REC = 1, DELETE_REC=2, SEARCH_DB=3, FIND_AVERAGE_EXP=4 ,REMOVE_DUPICATES=5,PRINT_DB=6,PRINT_PTRS=7,PRINT_DUPLICATES=8,QUIT_MENU=9,
	SEARH_BY_LVL=1, SEARCH_BY_NAME=2, QUIT_SEARCH=3
};

//function which prints menu into screen
bool printMainMenu(PLayersDatabase& p_database)
{
	cout << "Welcome to the database menu!" << endl;
	cout << "Press 1 to insert a new record at a particular position" << endl;
	cout << "      2 to delete a record from a particular position" << endl;
	cout << "      3 to search the database  and print results" << endl;
	cout << "      4 to find the average experience points of players at a particular level" << endl;
	cout << "      5 to find and remove all duplicate entries" << endl;
	cout << "      6 to print database" << endl;
	cout << "      7 to print pointers" << endl;
	cout << "      8 to print duplicates" << endl;
	cout << "      9 to quit" << endl;

	return getInputMainMenu(p_database);

}

//function which takes user input and performs changes on database 
bool getInputMainMenu(PLayersDatabase& p_database)
{
	int userChoice = userInput();

	if (userChoice == INSERT_NEW_REC)
	{
		p_database.insertRecordAtPos(recordCreation(), userInputWithText("Enter position."));
	}
	else if (userChoice == DELETE_REC)
	{
		p_database.deleteRecordAtPos(userInputWithText("Enter position for delete."));
	}
	else if (userChoice == SEARCH_DB)
	{
		printSearchMenu(p_database);
	}
	else if (userChoice == FIND_AVERAGE_EXP)
	{
	
		double average{ 0 }; 
		//the vector returned from function witch contains set of integers for further search function
		vector<int> resultVector = parsedInput(userInputStrWithText("Please enter level or level range in format (number lower - number higher) for which you want to calculate average experience."));
		if (resultVector[0] == -1)
		{
			cout << "Invalid input !" << endl;
		}
		else
		{
			if (resultVector.size() == 1)
			{
				average=p_database.getAverageExperience(resultVector[0]);
				cout << "Average experience points for level " << resultVector[0] << " are " << average << endl;
			}
			else
			{
				average=p_database.getAverageExperience(resultVector[0], resultVector[1]);
				cout << "Average experience points between levels " << resultVector[0] << " and " << resultVector[1]<< " are "  << average << endl;
			}
		}
		
	}
	else if (userChoice == REMOVE_DUPICATES)
	{
		p_database.removeDuplicates();
	}
	else if (userChoice == PRINT_DB)
	{
		p_database.printDatabase();
	}
	else if (userChoice == PRINT_PTRS)
	{
		p_database.printPointers();
	}
	else if (userChoice == PRINT_DUPLICATES)
	{
		p_database.printDuplicates();
	}
	else if (userChoice == QUIT_MENU)
	{
		return true;
	}
	else
	{
		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}

	return false;


}

//function which will create an record from user input
//order of the function is O(c)
PlayerRecord recordCreation()
{
	return PlayerRecord(userInputWithText("Enter experience"),
		userInputWithText("Enter level"),
		userInputStrWithText("Please Enter Last Name."),
		userInputStrWithText("Please Enter First Name."));
}

//function which prints menu into screen
void printSearchMenu(PLayersDatabase& p_database)
{
	cout << "Welcome to the search menu!" << endl;
	cout << "Press 1 to search by level" << endl;
	cout << "      2 to search by name" << endl;
	cout << "      3 to quit" << endl;

	getInputSearchMenu(p_database);

}


//function which takes user input and performs searches
void getInputSearchMenu(PLayersDatabase& p_database)
{
	int userChoice = userInput();

	if (userChoice == SEARH_BY_LVL)
	{
		//the vector returned from function witch contains set of integers for further search function
		vector<int> resultVector = parsedInput(userInputStrWithText("Please enter level or level range in format (number lower - number higher)"));
		if (resultVector[0] == -1)
		{
			cout << "Invalid input !" << endl;
		}
		else
		{
			if (resultVector.size() == 1)
			{
				p_database.printResultVector(p_database.searchByLevel(resultVector[0]));
			}
			else
			{
				p_database.printResultVector(p_database.searchByLevel(resultVector[0], resultVector[1]));
			}
		}
	}
	else if (userChoice == SEARCH_BY_NAME)
	{
		p_database.printResultVector(p_database.searchByName(userInputStrWithText("Enter name you want to find")));
	}
	else if (userChoice == QUIT_SEARCH)
	{
		return;
	}
	else
	{
		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}

	return;
}

































