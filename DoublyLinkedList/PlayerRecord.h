#ifndef PLAYER_RECORD_H
#define PLAYER_RECORD_H

#include<string>
#include<iostream>


class PlayerRecord
{
private:
	//default constant values if you change here all uses are changed
	static const int DEFAULT_LEVEL = 0;
	static const int DEFAULT_EXPERIENCE = 0;

	std::string m_firstName;
	std::string m_lastName;
	int m_pLevel;
	int m_experiencePoints;


public:
	PlayerRecord();
	PlayerRecord(int p_expPoints, int p_pLevel, std::string p_lName, std::string p_fName);

	//template function that will take any input but throws error if it is not a string or char*
	template<typename STR> // diference between class and typename http://stackoverflow.com/questions/2023977/c-difference-of-keywords-typename-and-class-in-templates
	PlayerRecord(STR&& p_fName, STR&& p_lName) : 
		m_firstName(std::forward<STR>(p_fName)),
		m_lastName(std::forward<STR>(p_lName)),
		m_pLevel(DEFAULT_LEVEL),
		m_experiencePoints(DEFAULT_EXPERIENCE)
	{
	}

	//perfect forwarding
	//templated constructor will speed up the construction by moving objects and not copying them also works for const and non const values
	//aplicable only for template functions and preserves arguments rvalunes or lvalunes implemented through forward
	template<typename STR1,typename STR2, typename INT>
	PlayerRecord(STR1&& p_fName, STR2&& p_lName, INT&& p_pLevel, INT&& p_expPoints) :
		m_firstName(std::forward<STR1>(p_fName)),
		m_lastName(std::forward<STR2>(p_lName)),
		m_pLevel(std::forward<INT>(p_pLevel)),
		m_experiencePoints(std::forward<INT>(p_expPoints))
	{
		//std::cout << "Called forward constructor" << std::endl;
	}

	~PlayerRecord();

	//getters and setters
	const std::string getFirstName() const;
	const std::string getLastName() const;
	int getPlayerLevel() const;
	int getExperience() const;

	template<typename T>
	void setFirstName(T&& p_fName)
	{
		m_firstName = std::forward<T>(p_fName);
	}

	template<typename T>
	void setLastName(T&& p_lastName)
	{
		m_lastName = std::forward<T>(p_lastName);
	}

	
	template<typename T>
	void setPlayerLevel(T&& p_levelToSet)
	{
		if (p_levelToSet < 0)
		{
			cout << "level canot be negative setting it to 0" << endl;
			m_pLevel = 0;
		}
		else
		{
			m_pLevel = std::forward<T>(p_levelToSet);
			//std::cout << "calling forward v" << std::endl;
		}

	}

	template<typename T>
	void setExpPoints(T&& p_expPointsToSet)
	{
		if (p_expPointsToSet < 0)
		{
			cout << "experience canot be negative setting it to 0" << endl;
			m_experiencePoints = 0;
		}
		else
		{
			m_experiencePoints = std::forward<T>(p_expPointsToSet);
		}
	}
	

	void print() const;


};




#endif;