//player record class implementation to populate database

#include"PlayerRecord.h"
#include<iostream>

using std::string;		using std::cout;
using std::endl;

PlayerRecord::PlayerRecord() :m_firstName("noFname"), m_lastName("noLastName"), m_pLevel(DEFAULT_LEVEL), m_experiencePoints(DEFAULT_EXPERIENCE)
{

}

//delegated constructor
PlayerRecord::PlayerRecord(int p_expPoints, int p_pLevel, std::string p_lName, std::string p_fName) : PlayerRecord(p_fName, p_lName, p_pLevel, p_expPoints)
{
	//std::cout << "Called normal constructor" << std::endl;
}

////delegated constructor
//PlayerRecord::PlayerRecord(int p_expPoints, int p_pLevel, std::string p_lName, std::string p_fName) : m_experiencePoints(p_expPoints), m_pLevel(p_pLevel), m_firstName(p_fName), m_lastName(p_lName)
//{
//	//std::cout << "Called normal constructor" << std::endl;
//}

PlayerRecord::~PlayerRecord()
{

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//getters and setters
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const string PlayerRecord::getFirstName() const
{
	return m_firstName;
}


const string PlayerRecord::getLastName() const
{
	return m_lastName;
}

int PlayerRecord::getPlayerLevel() const
{
	return m_pLevel;
}

int PlayerRecord::getExperience() const
{
	return m_experiencePoints;
}

void PlayerRecord::print() const
{
	//cout << "First Name: " << m_firstName << " Last Name: " << m_lastName << " Experience: " << m_experiencePoints << " Level: " << m_pLevel << endl;
	cout << m_firstName << "		" << m_lastName << "		" << m_experiencePoints << "	" << m_pLevel << endl;
}






























