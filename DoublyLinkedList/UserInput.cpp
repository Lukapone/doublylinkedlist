//functions which get the input from user
#include"UserInput.h"
#include<iostream>
#include<regex>
#include<sstream>//for checking strings into integers

using std::cin;		using std::cout;
using std::endl;	using std::string;
using std::vector;	using std::regex;
using std::regex_token_iterator; //this will iterate through the regex matches
using std::istringstream; //for checking convertion from string to int


//R"( string inside )" stands for raw strings literals in regex it means i dont need to escape \ in other words it will just write exactly what is inside so new line you write in new line
static const string REGEX_RANGE = R"((\d+)-(\d+))"; //this checks string if the pattern is correct number - number
static const string REGEX_DIGIT = R"(\d+)"; //this checks if the pattern is correct number

//puts > before user input
void expInput()
{
	cout << ">";
}

//function that takes input
int userInput()
{
	int input = 0;
	string inputToCheck = "";
	//this ads > before input
	expInput();
	cin >> inputToCheck;
	if (!(istringstream(inputToCheck) >> input))
	{
		//cout << "convertion from string to int failed please enter integer" << endl;
		input = userInputWithText("convertion from string to int failed please enter integer");

	}
	return input;
}

string userInputStr()
{
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}

//functions that takes input and displays what is expected from user
int userInputWithText(const string p_text)
{
	cout << p_text << endl;
	int input = 0;
	string inputToCheck = "";
	//this ads > before input
	expInput();
	cin >> inputToCheck;
	if (!(istringstream(inputToCheck) >> input))
	{
		//cout << "convertion from string to int failed please enter integer" << endl;
		input = userInputWithText("convertion from string to int failed please enter integer");

	}
	return input;
}

string userInputStrWithText(const string p_text)
{
	cout << p_text << endl;
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}

//function which will take string and parse digits out of the string works for formats (number) or (number-number) based on regular expresions
//the complexity is O(n) longer the string longer it takes
vector<int> parsedInput(string p_ToParse)	//http://www.cplusplus.com/reference/regex/regex_token_iterator/regex_token_iterator/
{
	regex rx(REGEX_RANGE);
	vector<int> toRet{}; // i know the size of vector wont be more than 2 
	toRet.reserve(2);
	int pushToVec{ -1 };
	if (regex_match(p_ToParse.begin(), p_ToParse.end(), rx)) //if the user input was correct extract numbers
	{
		int subMatches[] = { 1, 2 }; //this will pass array of submatches in regex
		regex_token_iterator<string::iterator> mMatched(p_ToParse.begin(), p_ToParse.end(), rx, subMatches); //separate the string into parts i want (gets just digits before '-' and after)
		regex_token_iterator<string::iterator> endMatches; //default is the end
		while (mMatched != endMatches)
		{
			if (!(istringstream(mMatched->str()) >> pushToVec))		// http://www.cplusplus.com/articles/D9j2Nwbp/
			{
				cout << "convertion from string to int failed in parseInput function vector gets -1 value" << endl;
				toRet.push_back(pushToVec); //just for easy debug not really necesarry
			}
			else
			{
				//cout << "convertion from string to int sucess value "<< pushToVec << endl;
				toRet.push_back(pushToVec);
			}
			mMatched++;
		}	
	}
	else if (regex_match(p_ToParse.begin(), p_ToParse.end(), rx=REGEX_DIGIT))
	{
		if (!(istringstream(p_ToParse) >> pushToVec))
		{
			cout << "convertion from string to int failed in parseInput function vector gets -1 value" << endl;
			toRet.push_back(pushToVec);
		}
		else
		{
			//cout << "convertion from string to int sucess value " << pushToVec << endl;
			toRet.push_back(pushToVec);
		}
	}
	else
	{
		toRet.push_back(pushToVec);
	}
	
	return toRet;
}



























