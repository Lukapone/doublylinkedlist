#ifndef DOUBLELISTNODE_H
#define DOUBLELISTNODE_H

#include<utility>
#include<iostream>
//DoubleListNode to model a node in list,

//Basic singly linked list node class //http://www.cplusplus.com/articles/E3wTURfi/
template <class Datatype>
class DoubleListNode
{
private:
	Datatype m_nodeData;
	DoubleListNode<Datatype>* m_nextNode;
	DoubleListNode<Datatype>* m_prevNode;

public:
	DoubleListNode();

	//move constructor which will forward what we be passed to it
	DoubleListNode(Datatype&& p_nodeData):
			m_nodeData(std::forward<Datatype>(p_nodeData)),
			m_prevNode(nullptr),
			m_nextNode(nullptr)
		{
			//std::cout << "using forward  constructor of node" << std::endl;
		}

	DoubleListNode(Datatype& p_nodeData);
	DoubleListNode(Datatype& p_nodeData, DoubleListNode<Datatype>* p_nextNode, DoubleListNode<Datatype>* p_prevNode);

	//move constructor which will forward what we will pass 
	DoubleListNode(Datatype&& p_nodeData, DoubleListNode<Datatype>* p_nextNode, DoubleListNode<Datatype>* p_prevNode) :
		m_nodeData(std::forward<Datatype>(p_nodeData)),
		m_prevNode(p_prevNode),
		m_nextNode(p_nextNode)
	{
			//std::cout << "using forward full constructor of node" << std::endl;
	}


	~DoubleListNode();

	
	Datatype& getData();

	//this will move the data if we call move in the function call and speeds up the seter
	void setData(Datatype&& p_newData)
	{
		m_nodeData = std::forward<Datatype>(p_newData);
		//std::cout << "Using forward &&" << std::endl;
	}

	void setData(Datatype& p_newData);
	DoubleListNode<Datatype>* getNextNode() const;
	void setNextNode(DoubleListNode<Datatype>* p_nodeToSet);
	DoubleListNode<Datatype>* getPrevNode() const;
	void setPrevNode(DoubleListNode<Datatype>* p_nodeToSet);

};

//default constructor
template <class Datatype>
DoubleListNode<Datatype>::DoubleListNode() : m_prevNode(nullptr), m_nextNode(nullptr)
{
}

//full constructor
template <class Datatype>
DoubleListNode<Datatype>::DoubleListNode(Datatype& p_nodeData) :
	m_nodeData(p_nodeData),
	m_prevNode(nullptr),
	m_nextNode(nullptr)
{
	//std::cout << "using copy constructor of node" << std::endl;
}

//full constructor
template <class Datatype>
DoubleListNode<Datatype>::DoubleListNode(Datatype& p_nodeData, DoubleListNode<Datatype>* p_nextNode, DoubleListNode<Datatype>* p_prevNode) :
m_nodeData(p_nodeData),
m_prevNode(p_prevNode), 
m_nextNode(p_nextNode)
{
}

//destructor
template <class Datatype>
DoubleListNode<Datatype>::~DoubleListNode()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//GETERS AND SETTERS all are O(c) complexity
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class Datatype>
Datatype& DoubleListNode<Datatype>::getData()
{
	return m_nodeData;
}

template <class Datatype>
void DoubleListNode<Datatype>::setData(Datatype& p_newData)
{
	m_nodeData = p_newData;
}

template <class Datatype>
DoubleListNode<Datatype>* DoubleListNode<Datatype>::getNextNode() const
{

	if (this == nullptr)//we have invalid node or last or first pointer
	{
		//std::cout << "null pointer" << std::endl;
		return nullptr;
	}
	return m_nextNode;
}

template <class Datatype>
void DoubleListNode<Datatype>::setNextNode(DoubleListNode<Datatype>* p_nodeToSet)
{
	m_nextNode = p_nodeToSet;
}

template <class Datatype>
DoubleListNode<Datatype>* DoubleListNode<Datatype>::getPrevNode() const
{
	if (this == nullptr)//we have invalid node or last or first pointer
	{
		//std::cout << "null pointer" << std::endl;
		return nullptr;
	}
	return m_prevNode;
}

template <class Datatype>
void DoubleListNode<Datatype>::setPrevNode(DoubleListNode<Datatype>* p_nodeToSet)
{
	m_prevNode = p_nodeToSet;
}



#endif