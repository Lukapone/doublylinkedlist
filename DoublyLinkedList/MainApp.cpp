//mainApp to test and run the database

#include<string>
#include<iostream>
//#include<regex>
#include<list>
#include<utility>

#define _CRTDBG_MAP_ALLOC //for memory leaks
#include"PlayersDatabase.h"
#include"MenuOptions.h"
#include"Timer.h"

using std::cout;		using std::cin;
using std::endl;		using std::string;
using std::vector;		using std::list;	//using std::regex;

void runDatabase();
//about std::move , std::forward , noexcept and threads https://www.youtube.com/watch?v=BezbcQIuCsY


int main()
{
#if defined(DEBUG)|defined(_DEBUG)
	//cout << "Getting debug mode" << endl;
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	Timer myTimer;
	lint myMs = myTimer.GetMS();

	runDatabase();



	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//demo vector speed performance
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//string mys{ "lukas" };
	//string mys1{ "ponik" };
	//int myIns{0};
	//int myInt{0};

	//myTimer.Reset();
	//PLayersDatabase testDatabase{};
	////vector<PlayerRecord> myVec;
	//vector<vector<string>> myVec{};
	//myVec.reserve(105); //add size to 10000 and see diference with reserve and without

	//for (int i = 0; i != 100000; i++)
	//{
	//	PlayerRecord recToPush("lukas", "ponik", 0, 0);
	//	//PlayerRecord recToPush(mys, mys1, myIns, myInt); //the speed for non templated constructor of 10000 elements 4143ms which is 4sec
	//													//the speed of templated function constructor of 10000 elements is 2414ms which is 2.4sec
	//	
	//	//PlayerRecord recToPush(myInt, myIns, mys, mys1); 
	//	//vector<string> recToPush(2000, "yey");

	//	//myVec.push_back(recToPush); //3.1sec without
	//	//myVec.push_back(std::move(recToPush)); //0.6 sec with move

	//	testDatabase.insertRecordAtEnd(recToPush);
	//}
	//
	//cout << myTimer.GetMS() << endl;
	//cout << mys << endl;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	cin.ignore();
	return 0;
}


void runDatabase()
{
	PLayersDatabase myData{};
	myData.populate();

	bool done = false;
	while (done == false)
	{
		done = printMainMenu(myData);
	}
}

