#ifndef DLITERATOR_H
#define DLITERATOR_H
//class that models bidirectional iterator which is used in doubly linked list class
#include<iostream>

template<class Datatype>
class DoublyLinkedList;

//custom iterator class for doubly linked list
template<class Datatype>
class DListIterator
{
private:
	typedef DoubleListNode<Datatype> doubleNode; 
	doubleNode* m_node;



public:

	DListIterator();
	DListIterator(doubleNode* p_node);
	~DListIterator();

	//overriden operators
	void operator++();    //prototype for  ++r; http://stackoverflow.com/questions/8318318/overload-operator
	void operator++(int); //prototype for  r++;
	void operator--();
	void operator--(int);
	bool operator==(const DListIterator<Datatype> other) const;
	bool operator!=(const DListIterator<Datatype> other) const;
	DListIterator<Datatype> operator+(const int p_num) const;
	Datatype& operator*() const;

	//getters and setters for node pointers
	doubleNode* getNodePtr() const;
	doubleNode* getPrevPtr() const;
	doubleNode* getNextPtr() const;
	void setPrevPtr(doubleNode* toSet);
	void setNextPtr(doubleNode* toSet);


	bool valid() const;
	Datatype& item() const;
	void printNodePtrs() const;

};



template<class Datatype>
DListIterator<Datatype>::DListIterator() : m_node(nullptr)
{
}

template<class Datatype>
DListIterator<Datatype>::DListIterator(doubleNode* p_node) : m_node(p_node)
{
}

template<class Datatype>
DListIterator<Datatype>::~DListIterator()
{
}

//all operator overloads in this case are O(c) constant complexity except + operator 
//moves the iterator
template<class Datatype>
void DListIterator<Datatype>::operator++()
{
	//std::cout << "calling ++ operator" << std::endl;
	m_node = m_node->getNextNode();
}

////moves the iterator
template<class Datatype>
void DListIterator<Datatype>::operator++(int)
{
	m_node = m_node->getNextNode();
}

////moves the iterator
template<class Datatype>
void DListIterator<Datatype>::operator--()
{
	m_node = m_node->getPrevNode();
}

////moves the iterator
template<class Datatype>
void DListIterator<Datatype>::operator--(int)
{
	m_node = m_node->getPrevNode();
}

//checks if the one node of one iterator equals second
template<class Datatype>
bool DListIterator<Datatype>::operator==(const DListIterator<Datatype> other) const
{
	return (m_node = other.m_node);
}

template<class Datatype>
bool DListIterator<Datatype>::operator!=(const DListIterator<Datatype> other) const
{
	return (m_node != other.m_node);
}

//function that will jumps number of records but it will returns null if it jumps more than it has
//complexity of the function is O(n) the more you will pass the longer it takes
template<class Datatype>
DListIterator<Datatype> DListIterator<Datatype>::operator+(const int p_num) const
{
	DListIterator<Datatype> iter = *this;
	for (int i = 0; i < p_num; i++)
	{
		if (iter.valid() == true) //If there's something to move onto...
		{
			iter++;
		}
		else
		{
			break;
		}			
	}
	
	if (iter.valid() == false)
	{
		std::cout << "iterator invalid returning null" <<std::endl;
		std::cin.ignore(); //easier to debug

	}

	return iter; //Return regardless of whether its valid...
}


//determines if the iterator is valid
//O(c) compexity
template<class Datatype>
bool DListIterator<Datatype>::valid() const
{
	return(m_node != nullptr);
}

//returns the item that iterator points to
//O(c) compexity
template<class Datatype>
Datatype& DListIterator<Datatype>::item() const
{
	return m_node->getData();
}

//returns the item that iterator points to
//O(c) compexity
template<class Datatype>
Datatype& DListIterator<Datatype>::operator*() const
{
	return m_node->getData();
}

//returns pointer to the current node
//O(c) compexity
template<class Datatype>
typename DListIterator<Datatype>::doubleNode* DListIterator<Datatype>::getNodePtr() const
{
	return m_node;
}

//prints the pointers of the current node 
//O(c) compexity
template<class Datatype>
void DListIterator<Datatype>::printNodePtrs() const
{
	std::cout << "Previous Node pointer		" <<m_node->getPrevNode() << std::endl;
	std::cout << "Node pointer						" << m_node<< std::endl;
	std::cout << "Next Node pointer		" << m_node->getNextNode() << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//getters and setters all are O(c) complexity
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class Datatype>
typename DListIterator<Datatype>::doubleNode* DListIterator<Datatype>::getPrevPtr() const
{
	return m_node->getPrevNode();
}



template<class Datatype>
typename DListIterator<Datatype>::doubleNode* DListIterator<Datatype>::getNextPtr() const
{
	return m_node->getNextNode();
}



template<class Datatype>
void DListIterator<Datatype>::setPrevPtr(doubleNode* toSet)
{
	m_node->setPrevNode(toSet);
}


template<class Datatype>
void DListIterator<Datatype>::setNextPtr(doubleNode* toSet)
{
	m_node->setNextNode(toSet);
}














#endif