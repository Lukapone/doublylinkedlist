//players database system with simple functionality

#include"PlayersDatabase.h"
#include<iostream>
#include<utility>
using std::vector;		using std::endl;
using std::cin;			using std::cout;

typedef DoublyLinkedList<PlayerRecord> recordList;
typedef vector<PlayerRecord> recordVector;

PLayersDatabase::PLayersDatabase()
{
}
PLayersDatabase::PLayersDatabase(recordList p_pRecords) :m_pRecords(p_pRecords)
{
}
PLayersDatabase::~PLayersDatabase()
{
}

//function which will populate database with testing records order of the function is O(c)
void PLayersDatabase::populate()
{
	//creating player records and populating my database with use of move because i wont need my records anymore
	PlayerRecord r1("Stefan", "Ponik", 22, 120);
	PlayerRecord r2("Lukas", "Ponik", 45, 250);
	PlayerRecord r3("Audreen", "Soh", 88, 3500);
	PlayerRecord r4("Anna", "Ponikov", 55, 8650);
	PlayerRecord r5("Filip", "Kresci", 2, 120);
	PlayerRecord r6("Anton", "Polom", 4, 1250);
	PlayerRecord r7("Peter", "Lipka", 33, 3333);
	PlayerRecord r8("Anna", "Lipkova", 85, 18250);
	PlayerRecord r9("Lukas", "Ponik", 55, 1250);
	PlayerRecord r10("Lukas", "Ponik", 45, 850);
	
	insertRecordAtEnd(std::move(r1));
	insertRecordAtEnd(std::move(r2));
	insertRecordAtEnd(std::move(r3));
	insertRecordAtEnd(std::move(r4));
	insertRecordAtEnd(std::move(r5));
	insertRecordAtEnd(std::move(r6));
	insertRecordAtEnd(std::move(r7));
	insertRecordAtEnd(std::move(r8));
	insertRecordAtEnd(std::move(r9));
	insertRecordAtEnd(std::move(r10));
}

//void PLayersDatabase::populate()
//{
//	//PlayerRecord r1("Stefan", "Ponik", 22, 120);
//	PlayerRecord r2("Lukas", "Ponik", 55, 2500);
//	PlayerRecord r3("Lukas", "Ponik", 55, 3500);
//	PlayerRecord r4("Lukas", "Ponik", 55, 2500);
//	//PlayerRecord r5("Filip", "Kresci", 2, 120);
//	//PlayerRecord r6("Anton", "Polom", 4, 1250);
//	//PlayerRecord r7("Peter", "Lipka", 33, 3333);
//	//PlayerRecord r8("Anna", "Lipkova", 85, 18250);
//	PlayerRecord r9("Lukas", "Ponik", 77, 3500);
//	//PlayerRecord r10("Lukas", "Kovalic", 45, 250);
//
//	//m_pRecords.insertEnd(r1);
//	m_pRecords.insertEnd(r2);
//	m_pRecords.insertEnd(r3);
//	m_pRecords.insertEnd(r4);
//	//m_pRecords.insertEnd(r5);
//	//m_pRecords.insertEnd(r6);
//	//m_pRecords.insertEnd(r7);
//	//m_pRecords.insertEnd(r8);
//	m_pRecords.insertEnd(r9);
//	//m_pRecords.insertEnd(r10);
//}

//gets us the reference of the records so we 
DoublyLinkedList<PlayerRecord> PLayersDatabase::getRecords() const
{
	return m_pRecords;
}

//function that calculate average experience based on level match next is overloaded version of same function
//the complexity to get average experience is O(n) the more players we have the longer it takes
double PLayersDatabase::getAverageExperience(int p_level) const
{
	//uniform initialization of variables
	double sum{ 0 };
	int count{0};

	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getPlayerLevel() == p_level)//if we have level we take experience
		{
			sum += iter.item().getExperience();
			count++;
		}
	}

	if (count == 0)
	{
		std::cout << "No players matched criteria " << std::endl;
		return 0;
	}

	return sum / count;
}

double PLayersDatabase::getAverageExperience(int p_levelMin, int p_levelMax) const
{
	double sum{ 0 };
	int count{0};

	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getPlayerLevel() >= p_levelMin && iter.item().getPlayerLevel() <= p_levelMax)//if we have level we take experience
		{
			sum += iter.item().getExperience();
			count++;
		}
	}

	if (count == 0)
	{
		std::cout << "No players matched criteria " << std::endl;
		return 0;
	}

	return sum / count;
}

//the function which takes level we want to search for and returns result of search in vector next is overloaded one
//complexity is O(n) more results we have longer it takes
vector<PlayerRecord> PLayersDatabase::searchByLevel(int p_level) const
{
	vector<PlayerRecord> resultSet;

	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getPlayerLevel() == p_level)//if we have level we want put into vector
		{
			resultSet.push_back(iter.item());
		}
	}

	if (resultSet.empty())
	{
		cout << "no results" << endl;
	}
	
	return resultSet;
}

vector<PlayerRecord> PLayersDatabase::searchByLevel(int p_levelMin, int p_levelMax) const
{
	vector<PlayerRecord> resultSet{};

	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getPlayerLevel() >= p_levelMin && iter.item().getPlayerLevel() <=p_levelMax)//if we have level we want put into vector
		{
			resultSet.push_back(iter.item());
		}
	}

	if (resultSet.empty())
	{
		cout << "no results" << endl;
	}

	return resultSet;
}

//the function which takes name we want to search for and returns result of search in vector next is overloaded one
//complexity is O(n) more results we have longer it takes
vector<PlayerRecord> PLayersDatabase::searchByName(std::string p_name) const
{
	vector<PlayerRecord> resultSet{};

	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getFirstName()==p_name)//if we have name we want put into vector
		{
			resultSet.push_back(iter.item());
		}
	}

	if (resultSet.empty())
	{
		cout << "no results" << endl;
	}

	return resultSet;

}

vector<PlayerRecord> PLayersDatabase::searchByName(std::string p_firstName, std::string p_lastName) const
{
	vector<PlayerRecord> resultSet{};

	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getFirstName() == p_firstName && iter.item().getLastName()==p_lastName)//if we have name we want put into vector
		{
			resultSet.push_back(iter.item());
		}
	}

	if (resultSet.empty())
	{
		cout << "no results" << endl;
	}

	return resultSet;
}

//prints the vector into screen
//complexity of the function is O(n) more records vector has the longer it takes
void PLayersDatabase::printResultVector(vector<PlayerRecord>& toPrint) const
{
	for (vector<PlayerRecord>::iterator iter = toPrint.begin(); iter != toPrint.end(); iter++)
	{
		iter->print();
	}
}

//prints all data in database
//complexity of the function is O(n) the more records are in database the longer it takes
void PLayersDatabase::printDatabase() const
{
	int count{0};
	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		cout << ++count << "	";
		iter.item().print();
		
	}
}

//prints pointers in each node in database
//complexity of the function is O(n)
void PLayersDatabase::printPointers() const
{
	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		iter.printNodePtrs();
	}
}

//functions which remove all the duplicates from database and keeps only one copy of each
void PLayersDatabase::removeDuplicates()
{
	for (recordVector::iterator iter = m_vecDuplicates.begin(); iter != m_vecDuplicates.end(); iter++)
	{
		this->removeDuplicatesByName(iter->getFirstName(), iter->getLastName());
	}
	m_vecDuplicates.clear(); //after we removed all duplicates we clear the vector
}


//function that checks database for duplicate names that user suplies
//complexity is O(n) more duplicates we have longer it takes
void PLayersDatabase::removeDuplicatesByName(std::string p_firstName, std::string p_lastName)
{
	int count{0};
	recordList::iterator tIter; //need this to not get the iterator invalid
	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
	{
		if (iter.item().getFirstName() == p_firstName && iter.item().getLastName() == p_lastName)//if we have name we want put into vector
		{
			if (count >= 1)
			{
				tIter = iter;
				iter++;
				m_pRecords.delIter(tIter);
				iter--; //move the iterator into position it should be
			}
			count++;
		}
	}
	cout << "There was " << count-1 << " duplicate(s) removed for name: "<< p_firstName<< " "<<p_lastName << endl;
}


////checks the records for duplicates and store the names in vectorOfDuplicates
////the complexity of the function is O(n) more to check longer it takes
//bool PLayersDatabase::checkForDuplicate(const PlayerRecord & p_toCheck)
//{
//	
//	//cout << "calling non auto check" << endl;
//	for (recordList::iterator iter = m_pRecords.begin(); iter != m_pRecords.end(); iter++)
//	{
//		if (iter.item().getFirstName() == p_toCheck.getFirstName() && iter.item().getLastName() == p_toCheck.getLastName())//if we have name we want put into vector
//		{
//			//cout << "found duplicate" << endl;
//			for (recordVector::iterator vIter = m_vecDuplicates.begin(); vIter != m_vecDuplicates.end(); vIter++)//if vector contains the duplicate dont add it again
//			{
//				if (vIter->getFirstName() == p_toCheck.getFirstName() && vIter->getLastName() == p_toCheck.getLastName())
//				{
//					//cout << "Vector contains that duplicate" << endl;
//					return true;
//				}
//				
//			}
//					m_vecDuplicates.push_back(p_toCheck);
//					//cout << "There was duplicate added into vector" <<endl;
//					return true;	
//		}
//	}
//	return false;
//}

//checks the records for duplicates and store the names in vectorOfDuplicates
//the complexity of the function is O(n) more to check longer it takes
bool PLayersDatabase::checkForDuplicate(const PlayerRecord & p_toCheck) //performance of the loop is almost identical with auto and normal iterator
{
	//cout << "calling auto check" << endl;
	
	for (const auto & player : m_pRecords)
	{
		if (player.getFirstName() == p_toCheck.getFirstName() && player.getLastName() == p_toCheck.getLastName())//if we have name we want put into vector
		{
			//cout << "found duplicate" << endl;
			for (auto & duplicate : m_vecDuplicates)//if vector contains the duplicate dont add it again
			{
				if (duplicate.getFirstName() == p_toCheck.getFirstName() && duplicate.getLastName() == p_toCheck.getLastName())
				{
					//cout << "Vector contains that duplicate" << endl;
					return true;
				}

			}
			m_vecDuplicates.push_back(p_toCheck);
			//cout << "There was duplicate added into vector" <<endl;
			return true;
		}
	}
	return false;
}

//the complexity of the function is O(c)
void PLayersDatabase::deleteRecordAtPos(int p_position)
{
	m_pRecords.deleteAtPosition(p_position);
}

void PLayersDatabase::printDuplicates() const
{
	for (const auto& duplicate : m_vecDuplicates)
	{
		duplicate.print();
	}
}



