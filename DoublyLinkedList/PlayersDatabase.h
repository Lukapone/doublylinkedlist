#ifndef PLAYERSDATABASE_H
#define PLAYERSDATABASE_H

#include<vector>
#include<string>

#include"DoublyLinkedList.h"
#include"PlayerRecord.h"
//this is a players database class i used to model database and its functionality it contains my doubly linked list of class PlayerRecord

class PLayersDatabase  //http://www.gotw.ca/gotw/006.htm using of const
{
private:
	DoublyLinkedList<PlayerRecord> m_pRecords;
	std::vector<PlayerRecord> m_vecDuplicates;


public:
	PLayersDatabase();
	PLayersDatabase(DoublyLinkedList<PlayerRecord> p_pRecords);
	~PLayersDatabase();

	template<class T>
	void insertRecordAtEnd(T&& p_toPush)
	{
		checkForDuplicate(p_toPush);
		m_pRecords.insertEnd(std::forward<T>(p_toPush));
	}

	template<class T>
	void insertRecordAtPos(T&& p_toPush, int p_position)
	{
		checkForDuplicate(p_toPush);
		m_pRecords.insertAtPosition(p_position, std::forward<T>(p_toPush));
	}

	
	bool checkForDuplicate(const PlayerRecord& toCheck);

	void deleteRecordAtPos(int p_position);
	std::vector<PlayerRecord> searchByLevel(int p_level) const;
	std::vector<PlayerRecord> searchByLevel(int p_levelMin,int p_levelMax) const;
	std::vector<PlayerRecord> searchByName(std::string p_name) const;
	std::vector<PlayerRecord> searchByName(std::string p_firstName, std::string p_lastName) const;
	void printResultVector(std::vector<PlayerRecord>& toPrint) const;
	double getAverageExperience(int p_level) const;
	double getAverageExperience(int p_levelMin, int p_levelMax) const;

	DoublyLinkedList<PlayerRecord> getRecords() const;

	void removeDuplicates();
	void removeDuplicatesByName(std::string p_firstName, std::string p_lastName);
	void populate();

	//print functionality
	void printPointers() const;
	void printDatabase() const;
	void printDuplicates() const;
};





#endif